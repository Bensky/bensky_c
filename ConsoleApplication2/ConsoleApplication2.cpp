#include <iostream>
#include "ConsoleApplication2.h"

//������� ����� Animal � ��������� ������� Voice() ������� ������� � ������� ������ � �������.
class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Some animal noise";
	}
};

//����������� �� Animal ������� ��� ������(� ������� Dog, Cat � �.�.) � � ��� ����������� ����� Voice() ����� �������, ����� ��� ������� � ������ Dog ����� Voice() ������� � ������� "Woof!".
class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!"; '\n';
	}
};

class Cat : public Animal
{
public:
	void Voice()  override
	{
		std::cout << "Meow!"; '\n';
	}
};

class Cow : public Animal
{
public:
	void Voice()  override
	{
		std::cout << "Mooo!"; '\n';
	}
};
	
//� ������� main ������� ������ ���������� ���� Animal � ��������� ���� ������ ��������� ��������� �������.
int main()
{
	Animal* farm[3]{ new Dog, new Cat, new Cow};

//����� �������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice().

	int i = 0;
	for (i = 0; i < 4; i++)
	{
		farm[i]->Voice();
	}
	
//�������������� ��� ������, ������ ���������� ��������� �� ����� ������� ����������� Animal
}